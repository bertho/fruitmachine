/*
 * Fruit Machine - The one armed bandit
 *
 * Copyright (C) 2014  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>

#include <defs.h>
#include <clock.h>

#define TMR0_PS		64UL					/* Timer prescaler 1:64 */
#define TMR0_FREQ	1000UL					/* Timer frequency */
#define TMR0CTC		(F_CPU/(TMR0_PS*TMR0_FREQ)-1UL)		/* CTC match value for frequency @ prescale*/

#define KEYPRESS_EV	0x10		/* Pressed a button */
#define KEYRELEASE_EV	0x20		/* Released a button */
#define TIMER_EV	0x30		/* A timer fired */
#define ENDROLL_EV	0x40		/* End of rolling */
#define MUSIC_EV	0x50		/* End of tune */
#define EV_MASK		0xf0

#define IS_EV_KEYPRESS(x)	(((x) & EV_MASK) == KEYPRESS_EV)	/* Any keypress event */
#define IS_EV_KEYPRESSx(x,y)	((x) == (KEYPRESS_EV | (y)))		/* Specific keypress event */
#define IS_EV_KEYRELEASE(x)	(((x) & EV_MASK) == KEYRELEASE_EV)	/* Any keyrelease event */
#define IS_EV_TIMER(x)		(((x) & EV_MASK) == TIMER_EV)		/* Any timer event */
#define IS_EV_TIMERx(x,y)	((x) == (TIMER_EV | (y)))		/* Specific timer event */
#define IS_EV_ENDROLL(x)	(((x) & EV_MASK) == ENDROLL_EV)		/* End-of-roll event */
#define IS_EV_MUSIC(x)		(((x) & EV_MASK) == MUSIC_EV)		/* End of music tune event */
#define EV_DATA(x)		((x) & ~EV_MASK)			/* Data from the event */

#define HOLDBLINK_TIME	125		/* Blink time interval of start/hold dots */
#define WINNING_TIME	150		/* Time between adding credits */
#define PAUSE_TIME	10000		/* Pause before scroll text begins */
#define SCROLL_TIME	250		/* Scroll interval */
#define ROTATE_TIME	70		/* Reel rolling interval */
#define SLEEP_TIME	60000		/* Time before we go to sleep */
#define SLEEPBLINK_TIME	2000		/* Low frequency blink to see we have power */
#define WAKING_TIME	4000		/* Button press length to wake up from sleep */

#define IS_HOLDING(x)	(holding & (1 << (x)))		/* Is a reel held? */

#define sound_off()	do { TCCR1A = _BV(COM1A1); /* Set clear output */ } while(0)

typedef union __dword_t {
	uint8_t		u8[4];
	uint16_t	u16[2];
	uint32_t	u32;
} dword_t;

uint8_t reelpos[4];			/* Current reel position in the reel[] array */
uint8_t reelposnext[4];			/* Next symbol in the reel[] array */
uint8_t reelcnt[4];			/* Roll count for reels */

enum {
	STATE_SLEEP,			/* All off, sleeping (most of the time) */
	STATE_WAKING,			/* Waking from sleep state */
	STATE_IDLE,			/* No credits idle state */
	STATE_MONEY,			/* Have credits, awaiting go */
	STATE_ROLLING,			/* Reels are rolling */
	STATE_WINTUNE,			/* A prize achieved, play a tune */
	STATE_WINNING,			/* Add prize credits to money */
	STATE_PLAYMUSIC,		/* No credits idle music/scroll text */
};

static uint8_t state;			/* Current state of game */
static uint8_t holding;			/* Reel holding indicators */
static uint8_t blinkcnt;		/* Dot blinking counter */
static uint8_t canhold;			/* Flag to indicate id reels can be held */
static uint8_t money;			/* Current credits */
static uint8_t prize;			/* Prize credits to add */
static uint8_t bonus;			/* Bonus flag (double-up) */

static const uint16_t *musictune;	/* Melody to play */
static uint8_t musicidx;		/* Melody index */
static uint8_t playonce;		/* Flag for single play or looping */
static uint8_t textidx;			/* Scroll text index */

static volatile uint8_t disps[6];	/* Display data */
static volatile uint8_t dim;		/* Dimmed display */
static volatile uint8_t dots;		/* Dots display data */
static volatile uint8_t dispidx;	/* Current display index in scan */

#define BUTTON_HOLD1	0
#define BUTTON_HOLD2	1
#define BUTTON_HOLD3	2
#define BUTTON_HOLD4	3
#define BUTTON_BONUS	4
#define BUTTON_START	5
#define BUTTON_MASK	0x0f
#define BUTTON_DEBOUNCE	15		/* Button debounce time in timer-interrupts */

static volatile uint8_t bcnt[6];	/* Button debounce counters */
static volatile uint8_t bstate;		/* Button state */
#define BUTTONS_ALLOFF()	(bstate == 0xfc)
#define BUTTONS_HAS(x)		(!(bstate & (1<<((x)+2))))

static uint32_t rand_val;		/* PRGN state */
/* PRNG */
static void lfsrrandom(void)
{
	uint8_t c;
	cli();
	c = rand_val & 1;
	rand_val >>= 1;
	if(c)
		rand_val ^= 0xa6a6a6a6;
	sei();
}

static uint8_t randval(void)
{
	lfsrrandom();
	return rand_val & 0xff;
}


#define NEVENT	(1<<3)			/* Event queue size */
static uint8_t events[NEVENT];		/* Event queue */
static uint8_t evstart;			/* Tail of queue index */
static uint8_t evend;			/* Head of queue index */

static void event_push(uint8_t ev)
{
	uint8_t sr = SREG;
	cli();
	if(((evstart+1) % NEVENT) == evend) {
		SREG = sr;
		return;
	}
	events[evstart++] = ev;
	evstart %= NEVENT;
	((uint8_t *)&rand_val)[2] ^= ADCL & 0x01;	/* ADC noise as random source (LSB only) */
	SREG = sr;
}

static uint8_t event_pop(void)
{
	uint8_t sr = SREG;
	uint8_t ev;
	cli();
	if(evstart == evend) {
		SREG = sr;
		return 0;
	}
	ev = events[evend++];
	evend %= NEVENT;

	if(IS_EV_KEYPRESS(ev) || IS_EV_KEYRELEASE(ev))
		((uint8_t *)&rand_val)[1] ^= TCNT2;	/* Mix event semi random into PRNG */

	SREG = sr;
	return ev;
}

#define CHECK_BUTTON(n)	do { \
				if(b & _BV((n)+2)) { \
					bcnt[(n)] = BUTTON_DEBOUNCE; \
				} else if(bcnt[(n)]) { \
					if(!--bcnt[(n)]) \
						event_push((n) | ((bstate & _BV((n)+2)) ? KEYRELEASE_EV : KEYPRESS_EV)); \
				} \
			} while(0)

/*
 * Timer interrupt handling:
 * - display scan
 * - button scan+debounce
 * - timer events
 */
ISR(TIMER0_COMPA_vect)
{
	uint8_t b, v;
	/* Update display */
	b = 1 << dispidx;
	PORTC = 0;			/* Turn off display */
	if((dim & (b | 0x80)))
		PORTD = v = disps[dispidx] | (dots & b ? 1 : 0);	/* Setup new segments */
	else
		PORTD = v = 0;
	if(v)
		PORTC = b;			/* Show the new display if there is data */
	if(!dispidx) {
		dispidx = 5;
		dim ^= 0x80;
	} else
		dispidx--;

	/* Check buttons */
	b = bstate;
	b ^= bstate = PINB & 0xfc;	/* Check buttons, masked with those we have */
	for(uint8_t i = 0; i < 6; i++) {
		CHECK_BUTTON(i);
	}

	/* Update timers */
	for(uint8_t i = 0; i < MAXTIMERS; i++) {
		if(clock_timers[i].state == TIMER_RUNNING) {
			if(!--clock_timers[i].cnt) {
				clock_timers[i].state = TIMER_EXPIRED;
				event_push(TIMER_EV | i);
			}
		}
	}

	ADCSRA |= _BV(ADSC);	/* Start an ADC conversion */
}

/* Initialize CPU and peripherals */
static void setup(void)
{
	cli();

	dispidx = 0;

	MCUCR = 0;			/* Standby mode, no external ints */

	PORTB = 0xfc;			/* Pull-up on buttons */
	PORTC = 0;
	PORTD = 0;

	DDRB = _BV(0) | _BV(1);		/* PB0 (unused) and PB1 (buzzer) as output */
	DDRC = 0x3f;			/* Port C is column drivers (displays) */
	DDRD = 0xff;			/* Port D is row drivers (segments) */

	TCCR0A = _BV(WGM01);		/* CTC mode */
	TCCR0B = _BV(CS01) | _BV(CS00);	/* timer0 prescaler 1:64 */
	OCR0A = TMR0CTC;		/* CTC value */

	TIMSK0 = _BV(OCIE0A);		/* Interrupt on compare A match */
	TCNT0 = 0;			/* Clear counter */

	sound_off();
	TCCR1B = _BV(WGM12) | _BV(CS10);
	OCR1A = pgm_read_word(&tone_pitch[TONE_C4]);
	TCNT1 = 0;

	/* Timer 2 is just spinning for random value */
	TCCR2A = 0;			/* Normal mode */
	TCCR2B = _BV(CS20);		/* timer2 prescaler 1:1 */

	ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(MUX3);	/* Vref=1.1V, ADC8 (temp) */
	ADCSRA = _BV(ADEN) | _BV(ADPS2) | _BV(ADPS1) | _BV(ADATE);	/* ADC enable, right adj, f/64 */
	ADCSRB = 0;					/* Free running ADC */

	evstart = evend = 0;
	bstate = PINB & 0xfc;

	rand_val = 0xdeadbeef;		/* PRNG cannot cope with zero */

	sei();				/* Enable interrupts */

	reelpos[0] = randval() % NREEL;	/* Initialize the reels */
	reelpos[1] = randval() % NREEL;
	reelpos[2] = randval() % NREEL;
	reelpos[3] = randval() % NREEL;
	reelposnext[0] = randval() % NREEL;
	reelposnext[1] = randval() % NREEL;
	reelposnext[2] = randval() % NREEL;
	reelposnext[3] = randval() % NREEL;
}

/* Play a sound at pith tone for dura milliseconds */
void play(uint8_t tone, uint16_t dura)
{
	if(!tone || tone > NTONE_PITCH) {
		sound_off();
		return;
	}
	clock_timer_set(TIMER_NOTE, dura);
	OCR1A = pgm_read_word(&tone_pitch[tone-1]);
	TCCR1A = _BV(COM1A0);	/* Set toggle output */
	TCNT1 = 0;
}

static uint8_t scroll_down(uint8_t v)
{
	return ((v & 0x80) ? 0x02 : 0) | ((v & 0x40) ? 0x20 : 0) | ((v & 0x04) ? 0x08 : 0) | ((v & 0x02) ? 0x10 : 0);
}

static uint8_t  scroll_up(uint8_t v)
{
	return ((v & 0x02) ? 0x80 : 0) | ((v & 0x20) ? 0x40 : 0) | ((v & 0x08) ? 0x04 : 0) | ((v & 0x10) ? 0x02 : 0);
}

static uint8_t scroll_mid(uint8_t a, uint8_t b)
{
	return ((a & 0x80) ? 0x10 : 0) | ((b & 0x10) ? 0x80 : 0);
}

/* Put reel data on display, also intermediate positions */
static void putreel(uint8_t r)
{
	uint8_t a, b;
	switch(reelcnt[r] & 0x03) {
	case 0:
		disps[r] = pgm_read_byte(&reel[reelpos[r]]);
		break;
	case 1:
		disps[r] = scroll_up(pgm_read_byte(&reel[reelposnext[r]]));
		break;
	case 2:
		a = pgm_read_byte(&reel[reelpos[r]]);
		b = pgm_read_byte(&reel[reelposnext[r]]);
		disps[r] = scroll_mid(a, b);
		break;
	case 3:
		disps[r] = scroll_down(pgm_read_byte(&reel[reelpos[r]]));
		break;
	}
}

/* Update the credits shown */
static void putmoney(uint8_t m)
{
	if(m >= 100)
		disps[4] = disps[5] = pgm_read_byte(&nums[9]);
	else {
		disps[4] = pgm_read_byte(&nums[m / 10]);
		disps[5] = pgm_read_byte(&nums[m % 10]);
	}
}

/* Play a melody */
static void tune_play(const uint16_t *t, uint8_t reps)
{
	playonce = reps;
	musictune = t;
	musicidx = 0;
	clock_timer_set(TIMER_MUSIC, 2);
}

/* Cancel melody play */
static void tune_cancel(void)
{
	musictune = NULL;
	clock_timer_clear(TIMER_MUSIC);
	sound_off();
}

/* Actions to perform when /entering/ a state */
static void state_enter(uint8_t s)
{
	uint8_t i;

	switch(s) {
	case STATE_SLEEP:
		clock_timer_set(TIMER_SLEEP, SLEEPBLINK_TIME);
		tune_cancel();
		disps[0] = 0;
		disps[1] = 0;
		disps[2] = 0;
		disps[3] = 0;
		disps[4] = 0;
		disps[5] = 0;
		dots = 0;
		money >>= 1;
		dim = 0;
		break;
	case STATE_WAKING:
		clock_timer_set(TIMER_WAKING, WAKING_TIME);
		dots = 1;
		break;
	case STATE_IDLE:
		clock_timer_set(TIMER_PAUSE, PAUSE_TIME);
		putmoney(money);
		putreel(0);
		putreel(1);
		putreel(2);
		putreel(3);
		bonus = 0;
		dots = 0;
		dim = 0;	/* Dim all segments */
		break;
	case STATE_PLAYMUSIC:
		tune_play(doewabass, 0);
		clock_timer_set(TIMER_SCROLL, SCROLL_TIME);
		clock_timer_set(TIMER_SLEEP, SLEEP_TIME);
		textidx = 0;
		break;
	case STATE_MONEY:
		clock_timer_set(TIMER_HOLDBLINK, HOLDBLINK_TIME);
		dim = 0x3f;	/* Un-dim all segments */
		if(money < 2) {
			bonus = 0;
			dots &= ~(1<<4);
		}
		break;
	case STATE_ROLLING:
		/* Setup reel counting for active reels */
		i = 8 << 2;
		if(!IS_HOLDING(0)) {
			reelcnt[0] = i;
			i += 2 << 2;
		}
		if(!IS_HOLDING(1)) {
			reelcnt[1] = i;
			i += 2 << 2;
		}
		if(!IS_HOLDING(2)) {
			reelcnt[2] = i;
			i += 2 << 2;
		}
		if(!IS_HOLDING(3)) {
			reelcnt[3] = i;
			i += 2 << 2;
		}
		dots = holding | (bonus ? (1<<4) : 0);
		clock_timer_set(TIMER_ROTATE, 2);
		break;
	case STATE_WINNING:
		clock_timer_set(TIMER_WINNING, WINNING_TIME);
		break;
	case STATE_WINTUNE:
		if(prize < 10) {
			tune_play(tune_prize_low, 1);
		} else if(prize <= 20) {
			tune_play(tune_prize_mid, 1);
		} else if(prize < 99) {
			tune_play(tune_prize_high, 1);
		} else {
			/* Jackpot */
			tune_play(tune_prize_jackpot, 1);
		}
		break;
	}
}

/* Actions to perform when /leaving/ a state */
static void state_leave(uint8_t s)
{
	switch(s) {
	case STATE_SLEEP:
		clock_timer_clear(TIMER_SLEEP);
	case STATE_WAKING:
		clock_timer_clear(TIMER_WAKING);
		break;
	case STATE_IDLE:
		clock_timer_clear(TIMER_PAUSE);
		break;
	case STATE_PLAYMUSIC:
		tune_cancel();
		clock_timer_clear(TIMER_SCROLL);
		clock_timer_clear(TIMER_SLEEP);
		break;
	case STATE_MONEY:
		clock_timer_clear(TIMER_HOLDBLINK);
		break;
	case STATE_ROLLING:
		dots = bonus ? (1<<4) : 0;
		holding = 0;
		break;
	case STATE_WINTUNE:
		tune_cancel();
		break;
	}
}

/* Change state of the machine */
static void state_set(uint8_t s)
{
	state_leave(state);
	state = s;
	state_enter(s);
}

/* Event handling based on state */
static void state_machine(uint8_t key)
{
	uint8_t i;

	switch(state) {
	case STATE_SLEEP:
		if(IS_EV_KEYPRESS(key))
			state_set(STATE_WAKING);
		else if(IS_EV_TIMERx(key, TIMER_SLEEP)) {
			dots ^= 0x01;
			clock_timer_set(TIMER_SLEEP, (dots & 0x01) ? SLEEPBLINK_TIME/20 : SLEEPBLINK_TIME);
		}
		break;

	case STATE_WAKING:
		if(IS_EV_KEYRELEASE(key) && BUTTONS_ALLOFF())
			state_set(STATE_SLEEP);
		else if(IS_EV_TIMERx(key, TIMER_WAKING)) {
			if(money) {
				putreel(0);
				putreel(1);
				putreel(2);
				putreel(3);
				bonus = 0;
				dots = 0;
				canhold = 0;
				holding = 0;
				putmoney(money);
				state_set(STATE_MONEY);
			} else
				state_set(STATE_IDLE);
		}
		break;

	case STATE_IDLE:
		if(IS_EV_KEYPRESSx(key, BUTTON_BONUS)) {
			if(BUTTONS_HAS(BUTTON_HOLD1) && BUTTONS_HAS(BUTTON_HOLD2) && BUTTONS_HAS(BUTTON_HOLD3) && BUTTONS_HAS(BUTTON_HOLD4)) {
				state_set(STATE_SLEEP);
				break;
			}
			putmoney(money = 10);
			canhold = 0;
			state_set(STATE_MONEY);
		} else if(IS_EV_KEYPRESSx(key, BUTTON_HOLD1)) {
			clock_timer_set(TIMER_WAKING, WAKING_TIME);
		} else if(IS_EV_TIMERx(key, TIMER_PAUSE)) {
			state_set(STATE_PLAYMUSIC);
		} else if(IS_EV_TIMERx(key, TIMER_WAKING) && BUTTONS_HAS(BUTTON_HOLD1)) {
			state_set(STATE_SLEEP);
		}
		break;

	case STATE_PLAYMUSIC:
		if(IS_EV_KEYPRESS(key)) {
			state_set(STATE_IDLE);
		} else if(IS_EV_TIMERx(key, TIMER_SCROLL)) {
			clock_timer_set(TIMER_SCROLL, SCROLL_TIME);
			for(i = 0; i < 6; i++) {
				disps[i] = pgm_read_byte(&scrolltext[(textidx+i)%NSCROLLTEXT]);
			}
			textidx++;
		} else if(IS_EV_TIMERx(key, TIMER_SLEEP)) {
			state_set(STATE_SLEEP);
		}
		break;

	case STATE_MONEY:
		if(IS_EV_KEYPRESSx(key, BUTTON_START)) {
			if(bonus)
				money -= 2;
			else
				money--;
			putmoney(money);
			dots = bonus ? (1<<4) : 0;
			blinkcnt = 0;
			state_set(STATE_ROLLING);
		} else if(IS_EV_KEYPRESSx(key, BUTTON_BONUS)) {
			if(BUTTONS_HAS(BUTTON_HOLD1) && BUTTONS_HAS(BUTTON_HOLD2) && BUTTONS_HAS(BUTTON_HOLD3) && BUTTONS_HAS(BUTTON_HOLD4)) {
				state_set(STATE_SLEEP);
				break;
			}
			if(money > 1) {
				bonus ^= 0x01;
				if(bonus) {
					dots |= (1<<4);
					play(TONE_A4, D_1_64);
				} else {
					dots &= ~(1<<4);
					play(TONE_G4, D_1_64);
				}
			}
		} else if(IS_EV_TIMERx(key, TIMER_HOLDBLINK)) {
			clock_timer_set(TIMER_HOLDBLINK, HOLDBLINK_TIME);
			if(blinkcnt & 0x01) {
				dots |= (1<<5);
				if(!bonus)
					dots &= ~(1<<4);
			} else {
				dots &= ~(1<<5);
				if(money > 1)
					dots |= (1<<4);
			}
			if(canhold) {
				if(blinkcnt & 0x02)
					dots |= 0x0f;
				else
					dots &= 0xf0 | holding;
			}
			blinkcnt++;
		} else if(canhold) {
			if(IS_EV_KEYPRESSx(key, BUTTON_HOLD1)) {
				holding ^= 0x01;
				if(IS_HOLDING(0)) {
					dots |= 0x01;
					play(TONE_E4, D_1_64);
				} else {
					dots &= ~0x01;
					play(TONE_C4, D_1_64);
				}
			} else if(IS_EV_KEYPRESSx(key, BUTTON_HOLD2)) {
				holding ^= 0x02;
				if(IS_HOLDING(1)) {
					dots |= 0x02;
					play(TONE_E4, D_1_64);
				} else {
					dots &= ~0x02;
					play(TONE_C4, D_1_64);
				}
			} else if(IS_EV_KEYPRESSx(key, BUTTON_HOLD3)) {
				holding ^= 0x04;
				if(IS_HOLDING(2)) {
					dots |= 0x04;
					play(TONE_E4, D_1_64);
				} else {
					dots &= ~0x04;
					play(TONE_C4, D_1_64);
				}
			} else if(IS_EV_KEYPRESSx(key, BUTTON_HOLD4)) {
				holding ^= 0x08;
				if(IS_HOLDING(3)) {
					dots |= 0x08;
					play(TONE_E4, D_1_64);
				} else {
					dots &= ~0x08;
					play(TONE_C4, D_1_64);
				}
			}
		}
		break;
	case STATE_ROLLING:
		if(IS_EV_TIMERx(key, TIMER_ROTATE)) {
			clock_timer_set(TIMER_ROTATE, ROTATE_TIME);
			play(TONE_C2, D_1_64);
			i = 0;
			if(reelcnt[0]) {
				if(!--reelcnt[0])
					play(TONE_C3, D_1_64);
				else
					i++;
				if(!(reelcnt[0] & 0x03)) {
					reelpos[0] = reelposnext[0];
					reelposnext[0] = randval() % NREEL;
				}
				putreel(0);
			}
			if(reelcnt[1]) {
				if(!--reelcnt[1])
					play(TONE_C3, D_1_64);
				else
					i++;
				if(!(reelcnt[1] & 0x03)) {
					reelpos[1] = reelposnext[1];
					reelposnext[1] = randval() % NREEL;
				}
				putreel(1);
			}
			if(reelcnt[2]) {
				if(!--reelcnt[2])
					play(TONE_C3, D_1_64);
				else
					i++;
				if(!(reelcnt[2] & 0x03)) {
					reelpos[2] = reelposnext[2];
					reelposnext[2] = randval() % NREEL;
				}
				putreel(2);
			}
			if(reelcnt[3]) {
				if(!--reelcnt[3])
					play(TONE_C3, D_1_64);
				else
					i++;
				if(!(reelcnt[3] & 0x03)) {
					reelpos[3] = reelposnext[3];
					reelposnext[3] = randval() % NREEL;
				}
				putreel(3);
			}
			if(!i) {
				clock_timer_clear(TIMER_ROTATE);
				event_push(ENDROLL_EV);
			}
		} else if(IS_EV_ENDROLL(key)) {
			uint8_t i = 0;
			dword_t r;
			r.u8[0] = pgm_read_byte(&reel[reelpos[0]]);
			r.u8[1] = pgm_read_byte(&reel[reelpos[1]]);
			r.u8[2] = pgm_read_byte(&reel[reelpos[2]]);
			r.u8[3] = pgm_read_byte(&reel[reelpos[3]]);
			dots = bonus ? (1<<4) : 0;
			canhold = holding == 0;
			holding = 0;
			/* Check winnings */
			while(1) {
				uint8_t v = pgm_read_byte(&prizes[i].value);
				if(!v) {
					state_set(money ? STATE_MONEY : STATE_IDLE);
					break;
				}
				dword_t p;
				p.u32 = pgm_read_dword(&prizes[i].reels[0]);
				if((!p.u8[0] || (p.u8[0] == r.u8[0])) &&
				   (!p.u8[1] || (p.u8[1] == r.u8[1])) &&
				   (!p.u8[2] || (p.u8[2] == r.u8[2])) &&
				   (!p.u8[3] || (p.u8[3] == r.u8[3]))) {
					/* We have a winning */
					dim = (p.u8[0] ? 0x01 : 0) | (p.u8[1] ? 0x02 : 0) | (p.u8[2] ? 0x04 : 0) | (p.u8[3] ? 0x08 : 0);
					prize = v;
					if(bonus)
						prize += v;
					canhold = 0;
					state_set(STATE_WINTUNE);
					break;
				}
				i++;
			}
		}
		break;

	case STATE_WINTUNE:
		if(IS_EV_MUSIC(key)) {
			state_set(STATE_WINNING);
		}
		break;

	case STATE_WINNING:
		if(IS_EV_TIMERx(key, TIMER_WINNING)) {
			if(++money > 99)
				money = 99;
			putmoney(money);
			play(TONE_A3, D_1_64);
			if(!--prize) {
				state_set(STATE_MONEY);
			} else {
				clock_timer_set(TIMER_WINNING, WINNING_TIME);
			}
		}
		break;

	default:
		state_set(STATE_IDLE);
		break;
	}
}

int main(void)
{
	uint8_t i;

	setup();

	state_set(STATE_IDLE);

	for(;;) {
		if((i = event_pop())) {
			/* Check for state independent timer events */
			if(IS_EV_TIMERx(i, TIMER_NOTE)) {
				sound_off();
				continue;
			} else if(IS_EV_TIMERx(i, TIMER_MUSIC)) {
				uint16_t note;
				if(!musictune)
					continue;	/* Race happens if cancelation gets a timer interrupt */
				note = pgm_read_word(&musictune[musicidx++]);
				if(!note) {
					event_push(MUSIC_EV);
					if(playonce)
						continue;
					musicidx = 1;
					note = pgm_read_word(&musictune[0]);
				}
				clock_timer_set(TIMER_MUSIC, _D(note) * TIMESTEP);
				play(_N(note), _D(note) * TIMESTEP - 1);
				continue;
			} else {
				/* If we get here, we have a state-dependent event to handle */
				state_machine(i);
			}
		}
		/* Go to sleep while we have nothing to do */
		PORTB |= 0x01;
		sleep_enable();
		sleep_cpu();
		PORTB &= ~0x01;
	}
}
