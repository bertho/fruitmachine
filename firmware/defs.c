/*
 * Fruit Machine - The one armed bandit
 *
 * Copyright (C) 2014  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdint.h>
#include <avr/pgmspace.h>

#include <defs.h>

const uint8_t reel[NREEL] PROGMEM = {
	SYM_O_BOT, SYM_O_BOT, SYM_O_BOT,
	SYM_O_TOP, SYM_O_TOP,
	SYM_BAR_TOP, SYM_BAR_TOP,
	SYM_BAR_MID, SYM_BAR_MID,
	SYM_BAR_BOT, SYM_BAR_BOT,
	SYM_VBAR_TOP,
	SYM_VBAR_MID,
	SYM_VBAR_BOT,
	SYM_TBAR,
	SYM_SEVEN,
};

/* Prize winning combinations, sorted from highest to lowest value */
const prize_t prizes[] PROGMEM = {
	PRIZE(    SYM_TBAR,     SYM_TBAR,     SYM_TBAR,     SYM_TBAR, 99),
	PRIZE(   SYM_SEVEN,    SYM_SEVEN,    SYM_SEVEN,    SYM_SEVEN, 50),
	PRIZE(SYM_VBAR_TOP, SYM_VBAR_TOP, SYM_VBAR_TOP, SYM_VBAR_TOP, 40),
	PRIZE(SYM_VBAR_MID, SYM_VBAR_MID, SYM_VBAR_MID, SYM_VBAR_TOP, 40),
	PRIZE(SYM_VBAR_BOT, SYM_VBAR_BOT, SYM_VBAR_BOT, SYM_VBAR_BOT, 40),
	PRIZE(    SYM_TBAR,     SYM_TBAR,     SYM_TBAR,            0, 30),
	PRIZE(           0,     SYM_TBAR,     SYM_TBAR,     SYM_TBAR, 30),
	PRIZE(   SYM_O_TOP,    SYM_O_TOP,    SYM_O_TOP,    SYM_O_TOP, 25),
	PRIZE(   SYM_SEVEN,    SYM_SEVEN,    SYM_SEVEN,            0, 25),
	PRIZE(           0,    SYM_SEVEN,    SYM_SEVEN,    SYM_SEVEN, 25),
	PRIZE( SYM_BAR_TOP,  SYM_BAR_TOP,  SYM_BAR_TOP,  SYM_BAR_TOP, 20),
	PRIZE( SYM_BAR_MID,  SYM_BAR_MID,  SYM_BAR_MID,  SYM_BAR_MID, 20),
	PRIZE( SYM_BAR_BOT,  SYM_BAR_BOT,  SYM_BAR_BOT,  SYM_BAR_BOT, 20),
	PRIZE(   SYM_O_BOT,    SYM_O_BOT,    SYM_O_BOT,    SYM_O_BOT, 15),
	PRIZE(           0, SYM_VBAR_TOP, SYM_VBAR_TOP, SYM_VBAR_TOP, 10),
	PRIZE(SYM_VBAR_TOP, SYM_VBAR_TOP, SYM_VBAR_TOP,            0, 10),
	PRIZE(           0, SYM_VBAR_MID, SYM_VBAR_MID, SYM_VBAR_MID, 10),
	PRIZE(SYM_VBAR_MID, SYM_VBAR_MID, SYM_VBAR_MID,            0, 10),
	PRIZE(           0, SYM_VBAR_BOT, SYM_VBAR_BOT, SYM_VBAR_BOT, 10),
	PRIZE(SYM_VBAR_BOT, SYM_VBAR_BOT, SYM_VBAR_BOT,            0, 10),
	PRIZE( SYM_BAR_TOP,  SYM_BAR_TOP,  SYM_BAR_TOP,            0,  8),
	PRIZE(           0,  SYM_BAR_TOP,  SYM_BAR_TOP,  SYM_BAR_TOP,  8),
	PRIZE( SYM_BAR_MID,  SYM_BAR_MID,  SYM_BAR_MID,            0,  8),
	PRIZE(           0,  SYM_BAR_MID,  SYM_BAR_MID,  SYM_BAR_MID,  8),
	PRIZE( SYM_BAR_BOT,  SYM_BAR_BOT,  SYM_BAR_BOT,            0,  8),
	PRIZE(           0,  SYM_BAR_BOT,  SYM_BAR_BOT,  SYM_BAR_BOT,  8),
//	PRIZE(           0,            0,     SYM_TBAR,     SYM_TBAR, 10),
	PRIZE(   SYM_O_TOP,    SYM_O_TOP,    SYM_O_TOP,            0,  6),
	PRIZE(           0,    SYM_O_TOP,    SYM_O_TOP,    SYM_O_TOP,  6),
	PRIZE(   SYM_O_BOT,    SYM_O_BOT,    SYM_O_BOT,            0,  5),
	PRIZE(           0,    SYM_O_BOT,    SYM_O_BOT,    SYM_O_BOT,  5),
//	PRIZE(           0,            0, SYM_VBAR_TOP, SYM_VBAR_TOP,  5),
//	PRIZE(           0,            0, SYM_VBAR_MID, SYM_VBAR_MID,  5),
//	PRIZE(           0,            0, SYM_VBAR_BOT, SYM_VBAR_BOT,  5),
//	PRIZE(           0,            0,  SYM_BAR_TOP,  SYM_BAR_TOP,  5),
//	PRIZE(           0,            0,  SYM_BAR_MID,  SYM_BAR_MID,  5),
//	PRIZE(           0,            0,  SYM_BAR_BOT,  SYM_BAR_BOT,  5),
	PRIZE(           0,            0,    SYM_O_TOP,    SYM_O_TOP,  3),
	PRIZE(           0,            0,    SYM_O_BOT,    SYM_O_BOT,  2),
//	PRIZE(           0,            0,            0,    SYM_O_TOP,  2),
//	PRIZE(           0,            0,            0,    SYM_O_BOT,  2),
	PRIZE(0, 0, 0, 0, 0)
};

/* Numerical values */
const uint8_t nums[] PROGMEM = {
	NUM_0, NUM_1, NUM_2, NUM_3, NUM_4,
	NUM_5, NUM_6, NUM_7, NUM_8, NUM_9,
};

/* Tone pitch table */
const uint16_t tone_pitch[NTONE_PITCH] PROGMEM = {
	61155, 57722, 54482, 51424, 48538, 45814, 43242, 40815, 38524, 36362, 34321, 32395,
	30577, 28860, 27240, 25711, 24268, 22906, 21620, 20407, 19261, 18180, 17160, 16197,
	15288, 14429, 13619, 12855, 12133, 11452, 10809, 10203, 9630, 9089 /* == 440Hz */, 8579, 8098,
	7643, 7214, 6809, 6427, 6066, 5725, 5404, 5101, 4814, 4544, 4289, 4048,
	3821, 3606, 3404, 3213, 3032, 2862, 2701, 2550, 2406, 2271, 2144, 2023,
	1910, 1802, 1701, 1606, 1515, 1430, 1350, 1274, 1202, 1135, 1071, 1011,
	954,
};

const uint8_t scrolltext[NSCROLLTEXT] PROGMEM = {
	CH_SP, CH_SP, CH_SP, CH_SP, CH_SP,
	CH_F, CH_R, CH_U, CH_I, CH_T, CH_SP, CH_M, CH_A, CH_C, CH_H, CH_I, CH_N, CH_E, CH_SP,
	CH_V, CH_A, CH_G, CH_R, CH_E, CH_A, CH_R, CH_G, CH_SP, NUM_2, NUM_0, NUM_1, NUM_4,
};

const uint16_t doewa[] PROGMEM = {
	_T(TONE_C5, D_1_8),
	_T(TONE_A4, D_1_8),
	_T(TONE_G4, D_1_8),
	_T(TONE_A4, D_3_8),
	_T(0, D_1_8),
	_T(TONE_C5, D_1_8),

	_T(TONE_Ds5, D_1_8),
	_T(TONE_Ds5, D_1_8),
	_T(TONE_D5, D_1_8),
	_T(TONE_Ds5, D_1_8),
	_T(TONE_D5, D_1_4),
	_T(TONE_C5, D_1_4),

	_T(TONE_C5, D_1_8),
	_T(TONE_A4, D_1_8),
	_T(TONE_G4, D_1_8),
	_T(TONE_A4, D_3_8),
	_T(0, D_1_8),
	_T(TONE_C5, D_1_8),

	_T(TONE_Ds5, D_1_8),
	_T(TONE_Ds5, D_1_8),
	_T(TONE_D5, D_1_8),
	_T(TONE_Ds5, D_1_8),
	_T(TONE_D5, D_1_4),
	_T(TONE_C5, D_1_4),

	_T(TONE_A4, D_1_4),
	_T(TONE_A4, D_3_4),
	_T(0, D_1_1),

	_T(0, 0),
};

const uint16_t doewabass[] PROGMEM = {
	_T(TONE_A2, D_1_16),
	_T(0, D_1_16),
	_T(TONE_A4, D_1_8),
	_T(TONE_G4, D_1_8),
	_T(TONE_A4, D_4_16),
	_T(TONE_E3, D_1_16),
	_T(0, D_3_16),
	_T(TONE_D3, D_1_16),
	_T(0, D_1_16),

	_T(TONE_Ds4, D_1_8),
	_T(TONE_Ds4, D_1_8),
	_T(TONE_D4, D_1_8),
	_T(TONE_C3, D_1_8),
	_T(TONE_D3, D_1_4),
	_T(TONE_E3, D_1_4),

	_T(0, 0),
};

const uint16_t tune_prize_low[] PROGMEM = {
	_T(TONE_B5, D_1_16),
	_T(TONE_C6, D_1_16),
	_T(TONE_B5, D_1_16),
	_T(TONE_G5, D_1_16),
//	_T(TONE_B5, D_1_16),
//	_T(TONE_C6, D_1_16),
//	_T(TONE_B5, D_1_16),
//	_T(TONE_G5, D_1_16),
	_T(0, 0),
};

const uint16_t tune_prize_mid[] PROGMEM = {
	_T(TONE_A2, D_1_16),
	_T(TONE_E3, D_1_16),
	_T(TONE_B3, D_1_16),
	_T(TONE_F4, D_1_16),
	_T(TONE_C5, D_1_16),
	_T(TONE_G5, D_1_16),
	_T(TONE_D6, D_1_16),
	_T(TONE_A7, D_1_16),
	_T(0, 0),
};

const uint16_t tune_prize_high[] PROGMEM = {
	_T(TONE_C4, D_1_8),
	_T(TONE_C4, D_1_16),
	_T(TONE_C4, D_1_16),
	_T(TONE_C4, D_1_16),
	_T(TONE_D4, D_1_8),
	_T(TONE_C4, D_1_8),
	_T(TONE_D4, D_1_8),
	_T(TONE_E4, D_1_2),
	_T(0, 0),
};

const uint16_t tune_prize_jackpot[] PROGMEM = {
	_T(TONE_A2, D_1_16),
	_T(TONE_E3, D_1_16),
	_T(TONE_B3, D_1_16),
	_T(TONE_F4, D_1_16),
	_T(TONE_C5, D_1_16),
	_T(TONE_G5, D_1_16),
	_T(TONE_D6, D_1_16),
	_T(TONE_A7, D_1_16),
	_T(TONE_D6, D_1_16),
	_T(TONE_G5, D_1_16),
	_T(TONE_C5, D_1_16),
	_T(TONE_F4, D_1_16),
	_T(TONE_B3, D_1_16),
	_T(TONE_E3, D_1_16),
	_T(TONE_A2, D_1_16),
	_T(TONE_E3, D_1_16),
	_T(TONE_B3, D_1_16),
	_T(TONE_F4, D_1_16),
	_T(TONE_C5, D_1_16),
	_T(TONE_G5, D_1_16),
	_T(TONE_D6, D_1_16),
	_T(TONE_A7, D_1_16),
	_T(TONE_D6, D_1_16),
	_T(TONE_G5, D_1_16),
	_T(TONE_C5, D_1_16),
	_T(TONE_F4, D_1_16),
	_T(TONE_B3, D_1_16),
	_T(TONE_E3, D_1_16),
	_T(TONE_A2, D_1_16),
	_T(TONE_E3, D_1_16),
	_T(TONE_B3, D_1_16),
	_T(TONE_F4, D_1_16),
	_T(TONE_C5, D_1_16),
	_T(TONE_G5, D_1_16),
	_T(TONE_D6, D_1_16),
	_T(TONE_A7, D_1_16),
	_T(0, 0),
};

