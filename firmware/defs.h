/*
 * Fruit Machine - The one armed bandit
 *
 * Copyright (C) 2014  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __FRUIT_DEFS_H
#define __FRUIT_DEFS_H

#define NELEM(x)	(sizeof(x)/sizeof((x)[0]))

#define setbit(port,bit)	do { port |= (uint8_t)_BV(bit); } while(0)
#define clrbit(port,bit)	do { port &= (uint8_t)~_BV(bit); } while(0)
#define tglbit(port,bit)	do { port ^= (uint8_t)_BV(bit); } while(0)

/*
 * 7-segments layout:
 *  aaaa
 * f    b
 * f    b
 *  gggg
 * e    c
 * e    c
 *  dddd  P
 *
 * Mapping to bits:
 * 76543210
 * abcdefgP
 */

/* Numbers in 7-segment form */
#define NUM_0	0xfc
#define NUM_1	0x60
#define NUM_2	0xda
#define NUM_3	0xf2
#define NUM_4	0x66
#define NUM_5	0xb6
#define NUM_6	0x3e
#define NUM_7	0xe0
#define NUM_8	0xfe
#define NUM_9	0xe6

/* Letters in 7-segment form */
#define CH_A	0xee
#define CH_B	0x3e
#define CH_C	0x1a
#define CH_D	0x75
#define CH_E	0x9e
#define CH_F	0x8e
#define CH_G	0xf6
#define CH_H	0x2e
#define CH_I	0x20
#define CH_J	0x70
#define CH_K	0x0e
#define CH_L	0x1c
#define CH_M	0xec
#define CH_N	0x2a
#define CH_O	0xfc
#define CH_P	0xce
#define CH_Q	0xfc
#define CH_R	0x0a
#define CH_S	0xb6
#define CH_T	0x1e
#define CH_U	0x38
#define CH_V	0x7c
#define CH_W	0x7c
#define CH_X	0x6e
#define CH_Y	0x76
#define CH_Z	0xda
#define CH_SP	0x00

#define SYM_O_TOP	0xc6	/* 'o' at bottom */
#define SYM_O_FULL	0xfc	/* 0 (zero) */
#define SYM_O_BOT	0x3a	/* 'o' at top */
#define SYM_BAR_TOP	0x80	/* Single horizontal bar at top */
#define SYM_BAR_MID	0x02	/* Single horizontal bar at middle */
#define SYM_BAR_BOT	0x10	/* Single horizontal bar at bottom */
#define SYM_DBAR_TOP	0x82	/* Dual horizontal bar at top */
#define SYM_DBAR_MID	0x90	/* Dual horizontal bar at top and bottom */
#define SYM_DBAR_BOT	0x12	/* Dual horizontal bar at bottom */
#define SYM_TBAR	0x92	/* Tripple horizontal bar */
#define SYM_VBAR_TOP	0x44	/* || at top */
#define SYM_VBAR_BOT	0x28	/* || at bottom */
#define SYM_VBAR_MID	0x6c	/* || over full height */
#define SYM_VBAR_LT	0x24	/* Left top | and right bottom | */
#define SYM_VBAR_RT	0x48	/* Right top | and left bottom | */
#define SYM_LBAR_MID	0x0c	/* | on whole left side */
#define SYM_RBAR_MID	0x60	/* | on whole right side */
#define SYM_PR		0x0e	/* Left | plus middle horizontal bar */
#define SYM_PL		0x62	/* Right | plus middle horizontal bar */
#define SYM_SEVEN	NUM_7
#define SYM_EIGHT	NUM_8


#define NTONE_PITCH	73	/* Number of pitches */

/* These are offset+1 into the pitch table */
#define TONE_C2		1
#define TONE_Cs2	2
#define TONE_Df2	TONE_Cs2
#define TONE_D2		3
#define TONE_Ds2	4
#define TONE_Ef2	TONE_Ds2
#define TONE_E2		5
#define TONE_F2		6
#define TONE_Fs2	7
#define TONE_Gf2	TONE_Fs2
#define TONE_G2		8
#define TONE_Gs2	9
#define TONE_Af2	TONE_Gs2
#define TONE_A2		10
#define TONE_As2	11
#define TONE_Bf2	TONE_As2
#define TONE_B2		12
#define TONE_C3		13
#define TONE_Cs3	14
#define TONE_Df3	TONE_Cs3
#define TONE_D3		15
#define TONE_Ds3	16
#define TONE_Ef3	TONE_Ds3
#define TONE_E3		17
#define TONE_F3		18
#define TONE_Fs3	19
#define TONE_Gf3	TONE_Fs3
#define TONE_G3		20
#define TONE_Gs3	21
#define TONE_Af3	TONE_Gs3
#define TONE_A3		22
#define TONE_As3	23
#define TONE_Bf3	TONE_As3
#define TONE_B3		24
#define TONE_C4		25
#define TONE_Cs4	26
#define TONE_Df4	TONE_Cs4
#define TONE_D4		27
#define TONE_Ds4	28
#define TONE_Ef4	TONE_Ds4
#define TONE_E4		29
#define TONE_F4		30
#define TONE_Fs4	31
#define TONE_Gf4	TONE_Fs4
#define TONE_G4		32
#define TONE_Gs4	33
#define TONE_Af4	TONE_Gs4
#define TONE_A4		34		/* This should be 440Hz */
#define TONE_As4	35
#define TONE_Bf4	TONE_As4
#define TONE_B4		36
#define TONE_C5		37
#define TONE_Cs5	38
#define TONE_Df5	TONE_Cs5
#define TONE_D5		39
#define TONE_Ds5	40
#define TONE_Ef5	TONE_Ds5
#define TONE_E5		41
#define TONE_F5		42
#define TONE_Fs5	43
#define TONE_Gf5	TONE_Fs5
#define TONE_G5		44
#define TONE_Gs5	45
#define TONE_Af5	TONE_Gs5
#define TONE_A5		46
#define TONE_As5	47
#define TONE_Bf5	TONE_As5
#define TONE_B5		48
#define TONE_C6		49
#define TONE_Cs6	50
#define TONE_Df6	TONE_Cs6
#define TONE_D6		51
#define TONE_Ds6	52
#define TONE_Ef6	TONE_Ds6
#define TONE_E6		53
#define TONE_F6		54
#define TONE_Fs6	55
#define TONE_Gf6	TONE_Fs6
#define TONE_G6		56
#define TONE_Gs6	57
#define TONE_Af6	TONE_Gs6
#define TONE_A6		58
#define TONE_As6	59
#define TONE_Bf6	TONE_As6
#define TONE_B6		60
#define TONE_C7		61
#define TONE_Cs7	62
#define TONE_Df7	TONE_Cs6
#define TONE_D7		63
#define TONE_Ds7	64
#define TONE_Ef7	TONE_Ds6
#define TONE_E7		65
#define TONE_F7		66
#define TONE_Fs7	67
#define TONE_Gf7	TONE_Fs6
#define TONE_G7		68
#define TONE_Gs7	69
#define TONE_Af7	TONE_Gs6
#define TONE_A7		70
#define TONE_As7	71
#define TONE_Bf7	TONE_As6
#define TONE_B7		72
#define TONE_C8		73

#define TIMESTEP	62
#define D_1_64	(TIMESTEP/2)
#define D_1_32	(TIMESTEP)
#define D_1_16	(2*D_1_32)
#define D_1_8	(2*D_1_16)
#define D_2_16	D_1_8
#define D_3_16	(3*D_1_16)
#define D_1_4	(2*D_1_8)
#define D_2_8	D_1_4
#define D_4_16	D_1_4
#define D_5_16	(5*D_1_16)
#define D_3_8	(3*D_1_8)
#define D_6_16	D_3_8
#define D_7_16	(7*D_1_16)
#define D_1_2	(2*D_1_4)
#define D_4_8	D_1_2
#define D_8_16	D_1_2
#define D_9_16	(9*D_1_16)
#define D_3_4	(3*D_1_4)
#define D_1_1	(2*D_1_2)

#define _T(p,d)	(((uint16_t)(p) << 8) | ((d)/TIMESTEP))
#define _N(n)	((n) >> 8)
#define _D(n)	((n) & 0xff)

#define NSCROLLTEXT	32	/* Number of chars in the scrolling text */
#define NREEL		16	/* Number of symbols on a reel */


/* Structure to hold prize-winning combinations with prize value */
typedef struct __prize_t {
	uint8_t	value;
	uint8_t	reels[4];
} prize_t;

#define PRIZE(a,b,c,d,v)	{ (v), { (a), (b), (c), (d) } }

extern const prize_t prizes[] PROGMEM;
extern const uint8_t reel[NREEL] PROGMEM;
extern const uint8_t nums[] PROGMEM;
extern const uint16_t tone_pitch[NTONE_PITCH] PROGMEM;
extern const uint8_t scrolltext[NSCROLLTEXT] PROGMEM;
extern const uint16_t doewa[] PROGMEM;
extern const uint16_t doewabass[] PROGMEM;
extern const uint16_t tune_prize_low[] PROGMEM;
extern const uint16_t tune_prize_mid[] PROGMEM;
extern const uint16_t tune_prize_high[] PROGMEM;
extern const uint16_t tune_prize_jackpot[] PROGMEM;

#endif
